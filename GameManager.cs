using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instancia{get; private set;}
    private Estados estados;
    [SerializeField] private GameObject perdistePanel;
    [SerializeField] private GameObject ganastePanel;
    private void Awake()
    {
        if (Instancia != null) Destroy(gameObject);
        else Instancia = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void ActualizarEstados(Estados nuevoEstado)
    {
        switch(nuevoEstado){
            case Estados.Jugando:
                break;
            case Estados.JuegoTerminado:
            perdistePanel.SetActive(true);
                // Debug.Log("Perdiste el juego.");
                break;
            case Estados.JuegoGanado:
            ganastePanel.SetActive(true);
                // Debug.Log("Ganaste el juego.");
                break;
            default:
                break;
        }
    }
}
public enum Estados
{
    Jugando,
    JuegoTerminado,
    JuegoGanado
}
