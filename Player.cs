using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    float velocidad = 4;
    //Serialize permite actuar desde el editor, si es private no se ve si es public si
    [SerializeField] private Transform pezSprite;
    // Start is called before the first frame update
    private int pecesComidos = 0;
    [SerializeField] private PlayerIA playerIA; 
    private float tamanio;
    void Start()
    {
        tamanio = transform.localScale.x;
    }

    // Update is called once per frame
    void Update()
    {
        //DESPLAZAMIENTO
        float inputHorizontal = Input.GetAxis("Horizontal") * Time.deltaTime * velocidad;
        float inputVertical = Input.GetAxis("Vertical") * Time.deltaTime * velocidad;

        transform.position = transform.position + new Vector3(inputHorizontal, inputVertical, 0);
        // Debug.Log(inputHorizontal + " - " + inputVertical);

        //PARA QUE NO SALGA DE LA PANTALLA, OBTENDREMOS LA VARIABLE DE LOS LIMITES CON LA CAMARA
        Vector2 limitesPantalla = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, limitesPantalla.x * -1, limitesPantalla.x),
        Mathf.Clamp(transform.position.y, limitesPantalla.y * -1, limitesPantalla.y), 0);

        //ROTACION
        if(inputHorizontal == 0) return;
        if(inputHorizontal < 0){ // esto indica que se esta despalzado hacia la izquierda xq es menor a 0
            pezSprite.rotation = Quaternion.Euler(new Vector3(0,180,0)); //esta devuelve una rotacion, esto trabaja con Z,X,Y
        }else{
            pezSprite.rotation = Quaternion.Euler(new Vector3(0,0,0));
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Pez"))
        {
            //COMPARAR EL TAMAÑO
            PezIA pezIA = collision.gameObject.GetComponent<PezIA>();
            if (tamanio >= pezIA.GetTamanio())
            {
            //ELIMINAR AL OTRO PEZ Y SUMAR UN PUNTO AL JUGADOR
                Destroy(collision.gameObject);
                pecesComidos++;
                playerIA.ActualizarPuntos(pecesComidos);
                
                if(pecesComidos >= 10){
                    GameManager.Instancia.ActualizarEstados(Estados.JuegoGanado);
                    velocidad = 0;
                }
                transform.localScale = transform.localScale + new Vector3(0.1f, 0.1f, 0.1f);
                tamanio = transform.localScale.x;
            }else{
                Destroy(gameObject);
                // Debug.Log("Game Over");
                GameManager.Instancia.ActualizarEstados(Estados.JuegoTerminado);
            }
        }
    } 
}
